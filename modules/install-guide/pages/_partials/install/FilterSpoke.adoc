
:experimental:

[[sect-installation-gui-installation-destination]]
=== Installation Destination - Specialized & Network Disks

This part of the `Installation Destination` screen allows you to configure non-local storage devices, namely iSCSI and FCoE storage. This section will mostly be useful to advanced users who have a need for networked disks. For instructions on setting up local hard drives, see xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-storage-partitioning[Installation Destination].

[IMPORTANT]
====

This section only explains how to make existing network disks available inside the installer. It does not explain how to set up your network or a storage server, only how to connect to them.

====

.Installation Destination - Network Storage Filters

image::anaconda/FilterSpoke.png[A list of currently configured network storage devices, displaying one configured iSCSI target.]

The screen contains a list of all currently available (discovered) network storage devices. When the screen is opened for the first time, the list will be empty in most cases because no network storage has been discovered - the installer makes no attempt at discovering this unless you configure network disks using a Kickstart file.

To add one or more storage devices to the screen so you can search them and use them in the installation, click `Add iSCSI Target` or `Add FCoE SAN` in the bottom right corner of the screen, and follow the instructions in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-destination-add-iscsi[Add iSCSI Target] or xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-destination-add-fcoe[Add FCoE SAN], depending on which type of network storage you want to add.

Network storage devices successfully discovered and configured by the installer will then be displayed in the main list, along with identifying information such as `Name`, `WWID`, `Model` and `Target`. To sort the list by a specific column (for example `WWID`), click the column's heading.

[NOTE]
====

On lower display resolutions, the list may be too wide to fit on the screen, and some of the columns or buttons may be hidden initially. Use the horizontal scroll bar at the bottom of the list to move your view and see all available table columns and controls.

====

There are three tabs on the top of the list, which display different information:

Search::  Displays all available devices, regardless of their type, and allows you to filter them either by their _World Wide Identifier_ (WWID) or by the port, target, or logical unit number (LUN) at which they are accessed.

Multipath Devices::  Storage devices accessible through more than one path, such as through multiple SCSI controllers or Fiber Channel ports on the same system.
+
[IMPORTANT]
====

The installation program only detects multipath storage devices with serial numbers that are 16 or 32 characters long.

====

Other SAN Devices::  Devices available on a Storage Area Network (SAN).

Depending on the tab you are currently in, you can filter the discovered devices by using the `Filter By` field. Some of the filtering options are automatically populated based on discovered devices (for example, if you select `Filter By:` `Vendor`, another drop-down menu will appear showing all vendors of all discovered devices). Other filters require your input (for example when filtering by WWID), and present you with a text input field instead of a drop-down menu.

In the list (regardless of how it is filtered), each device is presented on a separate row, with a check box to its left. Mark the check box to make the device available during the installation process; this will cause this device (node) to be shown in the `Specialized & Network Disks` section in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-storage-partitioning[Installation Destination]. There, you can select the disk as an installation target and proceed with either manual or automatic partitioning.

[NOTE]
====

Devices that you select here are not automatically wiped by the installation process. Selecting a device on this screen does not, in itself, place data stored on the device at risk. Also note that any devices that you do not select here to form part of the installed system can be added to the system after installation by modifying the `/etc/fstab` file.

====

When you have selected the storage devices to make available during installation, click `Done` to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-storage-partitioning[Installation Destination].

include::{partialsdir}/install/FilterSpoke_AddiSCSI.adoc[]

include::{partialsdir}/install/FilterSpoke_AddFCoE.adoc[]
